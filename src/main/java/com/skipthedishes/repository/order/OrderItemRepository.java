package com.skipthedishes.repository.order;

import com.skipthedishes.model.order.OrderItem;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderItemRepository extends MongoRepository<OrderItem, String>{

    @Query("{orderId: ?0}")
    List<OrderItem> getAll(long orderId);

    @Query("{'_id': ?0}")
    OrderItem get(long orderItemId);

}
