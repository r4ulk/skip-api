package com.skipthedishes.model.cousine;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document(collection = "cousine")
public class Cousine {

    private long id;

    private String name;

    public Cousine() {
    }

    public Cousine(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cousine cousine = (Cousine) o;
        return id == cousine.id &&
                Objects.equals(name, cousine.name);
    }

    @Override
    @Id
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Cousine{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
