package com.skipthedishes.service.store.impl;

import com.skipthedishes.model.store.Store;
import com.skipthedishes.repository.store.StoreRepository;
import com.skipthedishes.service.store.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StoreServiceImpl implements StoreService {

    private StoreRepository repository;

    @Autowired
    public StoreServiceImpl(StoreRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Store> getAll() {
        return repository.findAll();
    }

    @Override
    public List<Store> getAll(String searchText) {
        return repository.getAll(searchText);
    }

    @Override
    public Store get(long storeId) {
        return repository.get(storeId);
    }

    @Override
    public List<Store> getAll(long cousineId) {
        return repository.getAll(cousineId);
    }
}
