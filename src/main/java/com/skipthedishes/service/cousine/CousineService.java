package com.skipthedishes.service.cousine;

import com.skipthedishes.model.cousine.Cousine;

import java.util.List;

public interface CousineService {

    List<Cousine> getAll();

    List<Cousine> getAll(String searchText);

}
