package com.skipthedishes.controller.product;

import com.skipthedishes.service.product.ProductService;
import com.skipthedishes.model.product.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/Product")
public class ProductController {

    @Autowired
    ProductService service;

    /**
     * Get all Products Stored
     * @return List<Product> - The Product list
     */
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> getAll() {
        return service.getAll();
    }

    /**
     * Get all Products by search name
     * @param searchText - String to search
     * @return List<Product> - The Product list
     */
    @RequestMapping(value = "/search/{searchText}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> getAllBySearch(@PathVariable String searchText) {
        return service.getAll(searchText);
    }

    /**
     * Get Product by id
     * @param id - Long product identifier
     * @return Product - The Product
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Product get(@PathVariable long id) {
        return service.get(id);
    }

}
