package com.skipthedishes.repository.customer;

import com.skipthedishes.model.customer.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface CustomerRepository extends MongoRepository<Customer, String>{

    @Query("{'email': ?0, 'password': ?1}")
    Customer get(String email, String senha);

    @Query("{'_id': ?0}")
    Customer get(long customerId);

}
