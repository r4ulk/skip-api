package com.skipthedishes.repository.cousine;

import com.skipthedishes.model.cousine.Cousine;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CousineRepository extends MongoRepository<Cousine, String>{

    @Query("{'name':{$regex : ?0}}")
    List<Cousine> getAll(String nome);

}
