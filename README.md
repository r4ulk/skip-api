# README #

2018, March 18 - Vanhack / SKIP the Dishes Recruitment Fair
Challenge API for Recruiment Fair
This a Skip the Dishes API using spring-boot, mongodb and gradle for build project

### VanHackaton  Version ###

* Version TAG: 0.1.0

### Improvement AFTER VanHackaton ###

* Version TAG: 0.2.0

### How do I get set up? ###

First you need MongoDB installed: 

* Download: https://www.mongodb.com/download-center#community
* Configuration: https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/

Up mongo server, run:

`mongod`
 
 Up Spring-Boot application:
 
 `./gradlew build && java -jar build/libs/skip-api-0.1.0.jar`
 
### Swagger API Reference ###
 
* http://localhost:8080/api/v1/swagger-ui.html

### Credits ###
 
 Raul Klumpp - <raulklumpp@gmail.com>
