package com.skipthedishes.service.customer.impl;

import com.skipthedishes.model.customer.Customer;
import com.skipthedishes.repository.customer.CustomerRepository;
import com.skipthedishes.service.customer.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService, UserDetailsService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private CustomerRepository repository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository repository) {
        this.repository = repository;
    }

    @Override
    public Customer auth(String email, String password, String httpSessionId) {
        Customer customer = repository.get(email, password);
        if(customer != null) {
            Authentication authentication = new UsernamePasswordAuthenticationToken(customer.getName(), customer.getPassword());
            SecurityContextHolder.getContext().setAuthentication(authentication);
            customer.setToken(httpSessionId);
            logger.info("Login: {} successfully!", customer.getName());
            return customer;
        }
        return null;
    }

    @Override
    public Customer create(Customer customer) {
        return repository.insert(customer);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return null;
    }
}
