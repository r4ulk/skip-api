package com.skipthedishes.service.data.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skipthedishes.service.data.DataLoaderService;
import com.skipthedishes.model.cousine.Cousine;
import com.skipthedishes.model.product.Product;
import com.skipthedishes.model.store.Store;

import com.skipthedishes.repository.cousine.CousineRepository;
import com.skipthedishes.repository.product.ProductRepository;
import com.skipthedishes.repository.store.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DataLoaderServiceImpl implements DataLoaderService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private List<Cousine> cousines;

    private List<Store> stores;

    private List<Product> products;

    private CousineRepository cousineRepository;

    private StoreRepository storeRepository;

    private ProductRepository productRepository;

    @Autowired
    public DataLoaderServiceImpl(CousineRepository cousineRepository, StoreRepository storeRepository, ProductRepository productRepository) {
        this.cousineRepository = cousineRepository;
        this.storeRepository = storeRepository;
        this.productRepository = productRepository;
    }

    @Autowired
    ObjectMapper jsonMapper;

    @Override
    public void load() {
        this.loadCousines();;
        this.loadStores();
        this.loadProducts();
    }

    @Override
    public List<Store> getStores() {
        return this.stores;
    }

    @Override
    public List<Cousine> getCousines() {
        return this.cousines;
    }

    @Override
    public List<Product> getProducts() {
        return this.products;
    }

    private void loadStores() {
        TypeReference<List<Store>> typeReference = new TypeReference<List<Store>>() {
        };
        InputStream inputStream = TypeReference.class.getResourceAsStream("/stores.json");
        try {
            this.stores = jsonMapper.readValue(inputStream, typeReference);
            if (storeRepository.count() <= 0) {
                storeRepository.save(this.stores);
                logger.info("Stores data loaded...");
            }
        } catch (IOException e) {
            System.out.println(e);
            logger.error("Couldnt read stores from json. Message: ", e.getMessage());
        }
    }

    private void loadCousines() {
        TypeReference<List<Cousine>> typeReference = new TypeReference<List<Cousine>>() {
        };
        InputStream inputStream = TypeReference.class.getResourceAsStream("/cousines.json");
        try {
            this.cousines = jsonMapper.readValue(inputStream, typeReference);
            if (cousineRepository.count() <= 0) {
                cousineRepository.save(this.cousines);
                logger.info("Cousines data loaded...");
            }

        } catch (IOException e) {
            System.out.println(e);
            logger.error("Couldnt read cousines from json. Message: ", e.getMessage());
        }
    }

    private void loadProducts() {
        TypeReference<List<Product>> typeReference = new TypeReference<List<Product>>() {
        };
        InputStream inputStream = TypeReference.class.getResourceAsStream("/products.json");
        try {
            this.products = jsonMapper.readValue(inputStream, typeReference);
            if (productRepository.count() <= 0) {
                productRepository.save(this.products);
                logger.info("Products data loaded...");
            }
        } catch (IOException e) {
            System.out.println(e);
            logger.error("Couldnt read stores from json. Message: ", e.getMessage());
        }
    }
}
