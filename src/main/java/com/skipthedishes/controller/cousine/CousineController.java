package com.skipthedishes.controller.cousine;

import com.skipthedishes.model.cousine.Cousine;
import com.skipthedishes.model.store.Store;
import com.skipthedishes.service.cousine.CousineService;
import com.skipthedishes.service.store.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/Cousine")
public class CousineController {

    @Autowired
    CousineService service;

    @Autowired
    StoreService storeService;

    /**
     * Get all Cousines
     * @return List<Cousine> - The Cousine list
     */
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Cousine> getAll() {
        return service.getAll();
    }

    /**
     * Get all Cousines by search name
     * @param searchText - String to search
     * @return List<Cousine> - The Cousine list
     */
    @RequestMapping(value = "/search/{searchText}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Cousine> getAllBySearch(@PathVariable String searchText) {
        return service.getAll(searchText);
    }

    /**
     * Get all Stores of Cousine
     * @param cousineId - The Cousine identifier
     * @return List<Store> - The Store list of Cousine
     */
    @RequestMapping(value = "/{cousineId}/stores", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Store> getStores(@PathVariable long cousineId) {
        return storeService.getAll(cousineId);
    }
}
