package com.skipthedishes.model.order;

import com.skipthedishes.model.product.Product;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document(collection = "order-item")
public class OrderItem {

    private long id;

    private long orderId;

    private long productId;

    private Product product;

    private double price;

    private long quantity;

    private double total;

    public OrderItem() {
    }

    public OrderItem(long id, long orderId, long productId, Product product, double price, long quantity, double total) {
        this.id = id;
        this.orderId = orderId;
        this.productId = productId;
        this.product = product;
        this.price = price;
        this.quantity = quantity;
        this.total = total;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOrderId() {
        return orderId;
    }

    @Required
    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getProductId() {
        return productId;
    }

    @Required
    public void setProductId(long productId) {
        this.productId = productId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getPrice() {
        return price;
    }

    @Required
    public void setPrice(double price) {
        this.price = price;
    }

    public long getQuantity() {
        return quantity;
    }

    @Required
    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItem orderItem = (OrderItem) o;
        return id == orderItem.id &&
                orderId == orderItem.orderId &&
                productId == orderItem.productId;
    }

    @Override
    @Id
    public int hashCode() {
        return Objects.hash(id, orderId, productId);
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "id=" + id +
                ", orderId=" + orderId +
                ", productId=" + productId +
                ", product=" + product +
                ", price=" + price +
                ", quantity=" + quantity +
                ", total=" + total +
                '}';
    }

}
