package com.skipthedishes.controller.customer;

import com.skipthedishes.model.customer.Customer;
import com.skipthedishes.security.Attributes;
import com.skipthedishes.service.customer.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping(value = "/Customer")
public class CustomerController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    CustomerService service;

    /**
     * Authentication
     * @param email - The email string to auth
     * @param password - The password to auth
     * @return Customer - The Customer authenticated
     */
    @RequestMapping(value = "/auth", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Customer auth(@RequestParam String email, @RequestParam String password, HttpSession httpSession) {
        Customer customer = service.auth(email, password, httpSession.getId());
        httpSession.setAttribute(Attributes.CUSTOMER_KEY, customer);
        return customer;
    }

    /**
     * Create Customer
     * @param customer - The Customer
     * @return List<Cousine> - The Cousine list
     */
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Customer create(@RequestBody Customer customer) {
        return service.create(customer);
    }

}
