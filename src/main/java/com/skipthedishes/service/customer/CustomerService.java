package com.skipthedishes.service.customer;

import com.skipthedishes.model.customer.Customer;

public interface CustomerService {

    Customer auth(String email, String password, String httpSessionId);

    Customer create(Customer customer);

}
