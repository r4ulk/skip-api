package com.skipthedishes.model.customer;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Document(collection = "customer")
public class Customer {

    @Id
    private long id;

    private String email;

    private String name;

    private String address;

    private Date creation;

    private String password;

    private String token;


    public Customer() {
    }

    public Customer(long id, String email, String name, String address, String password) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.address = address;
        this.creation = new Date();
        this.password = password;
    }

    public Customer(long id, String email, String name, String address, Timestamp creation, String password) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.address = address;
        this.creation = (creation == null) ? new Date() : creation;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    @Required
    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    @Required
    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreation() {
        return creation;
    }

    public void setCreation(Date creation) {
        this.creation = creation;
    }

    public String getPassword() {
        return password;
    }

    @Required
    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(id, customer.id) &&
                Objects.equals(email, customer.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", creation=" + creation +
                ", password='" + password + '\'' +
                '}';
    }
}
