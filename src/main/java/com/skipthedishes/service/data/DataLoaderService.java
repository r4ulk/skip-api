package com.skipthedishes.service.data;

import com.skipthedishes.model.cousine.Cousine;
import com.skipthedishes.model.product.Product;
import com.skipthedishes.model.store.Store;

import java.util.List;

public interface DataLoaderService {

    void load();

    List<Store> getStores();

    List<Cousine> getCousines();

    List<Product> getProducts();
}
