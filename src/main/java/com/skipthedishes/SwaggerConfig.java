package com.skipthedishes;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;
import static com.google.common.base.Predicates.or;

import com.google.common.base.Predicate;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket postsApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("public-api")
                .apiInfo(apiInfo()).select().apis(RequestHandlerSelectors.basePackage("com.skipthedishes.controller")).build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("Skip The Dishes - API")
                .description("API reference for developers")
                .termsOfServiceUrl("http://localhost:8080/api/v1/")
                .contact("raulklumpp@gmail.com").license("Skip The Dishes License")
                .licenseUrl("raulklumpp@gmail.com").version("0.2.0").build();
    }

}