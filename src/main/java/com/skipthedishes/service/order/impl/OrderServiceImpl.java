package com.skipthedishes.service.order.impl;

import com.skipthedishes.model.order.Order;
import com.skipthedishes.repository.order.OrderRepository;
import com.skipthedishes.service.order.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private OrderRepository repository;

    @Autowired
    public OrderServiceImpl(OrderRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Order> getAll(long customerId) {
        return repository.getAll(customerId);
    }

    @Override
    public Order get(long orderId) {
        return repository.get(orderId);
    }

    @Override
    public Order create(Order order) {
        return repository.insert(order);
    }

}
