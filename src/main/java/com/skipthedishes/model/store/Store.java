package com.skipthedishes.model.store;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document(collection = "store")
public class Store {

    private long id;

    private String name;

    private String address;

    private long cousineId;

    public Store() {
    }

    public Store(long id, String name, String address, long cousineId) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.cousineId = cousineId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getCousineId() {
        return cousineId;
    }

    public void setCousineId(long cousineId) {
        this.cousineId = cousineId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Store store = (Store) o;
        return id == store.id &&
                cousineId == store.cousineId &&
                Objects.equals(name, store.name);
    }

    @Override
    @Id
    public int hashCode() {
        return Objects.hash(id, name, cousineId);
    }

    @Override
    public String toString() {
        return "Store{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", cousineId=" + cousineId +
                '}';
    }
}
