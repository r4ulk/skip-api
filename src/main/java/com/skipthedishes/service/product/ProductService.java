package com.skipthedishes.service.product;

import com.skipthedishes.model.product.Product;

import java.util.List;

public interface ProductService {

    List<Product> getAll();

    List<Product> getAll(String search);

    Product get(long id);

    List<Product> getAll(long storeId);

}
