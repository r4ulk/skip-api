package com.skipthedishes.controller.order;

import com.skipthedishes.model.customer.Customer;
import com.skipthedishes.model.order.Order;
import com.skipthedishes.service.order.OrderService;
import com.skipthedishes.security.Attributes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping(value = "/Order")
public class OrderController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    OrderService service;

    /**
     * Get Order by id
     *
     * @param orderId - Long order identifier
     * @return Order - The Order
     */
    @RequestMapping(value = "/{orderId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Order get(@PathVariable long orderId) {
        return service.get(orderId);
    }

    /**
     * Create Order
     * @param order - Order the order with order items
     * @return Order - Ther Order created
     */
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Order create(@RequestBody Order order) {
        return service.create(order);
    }

    /**
     * Get all Orders by Customer
     * @return List<Order> - The Order list
     */
    @RequestMapping(value = "/customer", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Order> getAll(HttpSession httpSession) {
        Customer customer = (Customer) httpSession.getAttribute(Attributes.CUSTOMER_KEY);
        return service.getAll(customer.getId());
    }

}

