package com.skipthedishes.service.product.impl;

import com.skipthedishes.model.product.Product;
import com.skipthedishes.repository.product.ProductRepository;
import com.skipthedishes.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository repository;

    @Autowired
    public ProductServiceImpl(ProductRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Product> getAll() {
        return repository.findAll();
    }

    @Override
    public List<Product> getAll(String searchText) {
        return repository.getAll(searchText);
    }

    @Override
    public Product get(long productId) {
        return repository.get(productId);
    }

    @Override
    public List<Product> getAll(long storeId) {
        return repository.getAll(storeId);
    }
}
