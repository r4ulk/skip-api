package com.skipthedishes.service.store;

import com.skipthedishes.model.store.Store;

import java.util.List;

public interface StoreService {

    List<Store> getAll();

    List<Store> getAll(String searchText);

    Store get(long storeId);

    List<Store> getAll(long cousineId);

}
