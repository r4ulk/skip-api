package com.skipthedishes.controller.store;

import com.skipthedishes.model.product.Product;
import com.skipthedishes.model.store.Store;
import com.skipthedishes.service.product.ProductService;
import com.skipthedishes.service.store.StoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/Store")
public class StoreController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    StoreService service;

    @Autowired
    ProductService productService;

    /**
     * Get all Stores
     * @return List of Stores
     */
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Store> getAll() {
        return service.getAll();
    }

    /**
     * Get Store by id
     * @param storeId - Long store identifier
     * @return List<Store> - The Store list
     */
    @RequestMapping(value = "/{storeId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Store getById(@PathVariable("storeId") long storeId) {
        return service.get(storeId);
    }

    /**
     * Get all Stores by search name
     * @param searchText - String to search
     * @return List<Store> - The Store list
     */
    @RequestMapping(value = "/search/{searchText}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Store> getAllBySearch(@PathVariable String searchText) {
        return service.getAll(searchText);
    }

    /**
     * Get all Products of Store
     * @param storeId - The Store identifier
     * @return List<Product> - The Product list of Store
     */
    @RequestMapping(value = "/{storeId}/products", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> getProducts(@PathVariable long storeId) {
        return productService.getAll(storeId);
    }
}
