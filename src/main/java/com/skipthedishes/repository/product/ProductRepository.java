package com.skipthedishes.repository.product;

import com.skipthedishes.model.product.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends MongoRepository<Product, String>{

    @Query("{'name':{$regex : ?0}}")
    List<Product> getAll(String nome);

    @Query("{'_id': ?0}")
    Product get(long productId);

    @Query("{'storeId': ?0}")
    List<Product> getAll(long storeId);

}
