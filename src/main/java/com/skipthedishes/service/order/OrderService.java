package com.skipthedishes.service.order;

import com.skipthedishes.model.order.Order;

import java.util.List;

public interface OrderService {

    List<Order> getAll(long customerId);

    Order get(long orderId);

    Order create(Order order);

}
