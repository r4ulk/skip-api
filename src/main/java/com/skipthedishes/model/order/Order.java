package com.skipthedishes.model.order;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Document(collection = "order")
public class Order {

    private long id;

    private Date date;

    private long customerId;

    private String deliveryAddress;

    private String contact;

    private long storeId;

    private List<OrderItem> orderItems;

    private double total;

    private String status;

    private Date lastUpdate;

    public Order() {
    }

    public Order(long id, Date date, long customerId, String deliveryAddress, String contact, long storeId,
                 List<OrderItem> orderItems, double total, String status, Date lastUpdate) {
        this.id = id;
        this.date = date;
        this.customerId = customerId;
        this.deliveryAddress = deliveryAddress;
        this.contact = contact;
        this.storeId = storeId;
        this.orderItems = orderItems;
        this.total = total;
        this.status = status;
        this.lastUpdate = lastUpdate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    @Required
    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getContact() {
        return contact;
    }

    @Required
    public void setContact(String contact) {
        this.contact = contact;
    }

    public long getStoreId() {
        return storeId;
    }

    @Required
    public void setStoreId(long storeId) {
        this.storeId = storeId;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    @Required
    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    @Required
    public void setStatus(String status) {
        this.status = status;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id &&
                customerId == order.customerId &&
                Objects.equals(deliveryAddress, order.deliveryAddress);
    }

    @Override
    @Id
    public int hashCode() {
        return Objects.hash(id, customerId, deliveryAddress);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", date=" + date +
                ", customerId=" + customerId +
                ", deliveryAddress='" + deliveryAddress + '\'' +
                ", contact='" + contact + '\'' +
                ", storeId=" + storeId +
                ", orderItems=" + orderItems +
                ", total=" + total +
                ", status='" + status + '\'' +
                ", lastUpdate=" + lastUpdate +
                '}';
    }
}
