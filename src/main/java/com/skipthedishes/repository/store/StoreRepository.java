package com.skipthedishes.repository.store;

import com.skipthedishes.model.store.Store;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StoreRepository extends MongoRepository<Store, String>{

    @Query("{'name':{$regex : ?0}}")
    List<Store> getAll(String nome);

    @Query("{cousineId: ?0}")
    List<Store> getAll(long cousineId);

    @Query("{'_id': ?0}")
    Store get(long storeId);

}
