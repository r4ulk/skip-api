package com.skipthedishes.service.cousine.impl;

import com.skipthedishes.model.cousine.Cousine;
import com.skipthedishes.repository.cousine.CousineRepository;
import com.skipthedishes.service.cousine.CousineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CousineServiceImpl implements CousineService {

    private CousineRepository repository;

    @Autowired
    public CousineServiceImpl(CousineRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Cousine> getAll() {
        return repository.findAll();
    }

    @Override
    public List<Cousine> getAll(String searchText) {
        return repository.getAll(searchText);
    }

}
