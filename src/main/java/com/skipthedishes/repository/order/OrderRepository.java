package com.skipthedishes.repository.order;

import com.skipthedishes.model.order.Order;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends MongoRepository<Order, String>{

    @Query("{customerId: ?0}")
    List<Order> getAll(long customerId);

    @Query("{'_id': ?0}")
    Order get(long orderId);

}
